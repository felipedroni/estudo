import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ContasPage } from '../pages/contas/contas';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  raiz = ContasPage;

  pages: Array<{title: string, component: any}>;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {

  this.pages = [
    { title: 'Home', component: HomePage },
    { title: 'Contas', component: ContasPage }
  ];

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });

  }

  openPage(page) {
      this.nav.setRoot(page.component);
    }

}
