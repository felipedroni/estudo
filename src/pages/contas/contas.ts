import { Component } from '@angular/core';
import { DAOContas } from 'C:/Users/Felipe/App/Financeiro/src/dao/dao-contas.js';

@Component({
  selector: 'page-contas',
  templateUrl: 'contas.html'
})
export class ContasPage {

  constructor() {

    this.dao = new DAOContas();
    this.listContas = this.dao.getList();

  }

}
